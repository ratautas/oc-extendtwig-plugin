<?php

namespace Ratauto\ExtendTwig;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name' => 'Extend Twig',
            'description' => 'Extends Twig with handy filters.',
            'author' => 'Algirdas Tamasauskas',
        ];
    }

    public function registerMarkupTags()
    {
        return [
            'filters' => [
                'slugify' => [$this, 'slugify'],
                'chunkify' => [$this, 'chunkify'],
            ],
        ];
    }

    public function slugify($text)
    {
        $slugify = new Slugify();

        return $slugify->slugify($text);
    }

    public function chunkify($items, $chunkSize)
    {
        $i = 0;
        $chunkedItems = array();
        foreach ($items as $item) {
            $chunk = $i % $chunkSize;
            if (!isset($chunkedItems[$chunk])) {
                $chunkedItems[$chunk] = array();
            }
            array_push($chunkedItems[$chunk], $item);
            ++$i;
        }

        return $chunkedItems;
    }
}
